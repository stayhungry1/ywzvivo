import cv2
import numpy as np

import matplotlib.pyplot as plt
# X = [[1, 2], [3, 4], [5, 6]]
# plt.imshow(X)
# plt.colorbar()


f1 = "ori.png"
f2 = "out.png"


png1 = cv2.imread(f1, -1).astype(np.float32)
png2 = cv2.imread(f2, -1).astype(np.float32)
print(png2.shape)
print((png1.max()))
print((png2.max()))
print(png1)
print(png2)

plt.subplot(121)
plt.imshow(png1)
plt.colorbar()
plt.subplot(122)
plt.imshow(png2)
plt.colorbar()
plt.show()


# row, col = 0, 4350
row, col = 0, 0
print("at:[{},{}], png1:{},png2:{}".format(row, col, png1[row, col], png2[row, col]))