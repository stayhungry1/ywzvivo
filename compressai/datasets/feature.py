# Copyright (c) 2021-2022, InterDigital Communications, Inc
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the disclaimer
# below) provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# * Neither the name of InterDigital Communications, Inc nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.

# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
# THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
# NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from pathlib import Path
import random

from PIL import Image
from torch.utils.data import Dataset
from .parse_p2_feature import *
from .parse_p2_feature import _joint_split_features
# from .utils import * #合图降通道


class FeatureFolder(Dataset):
    """Load an image folder database. Training and testing image samples
    are respectively stored in separate directories:

    .. code-block::

        - rootdir/
            - train/
                - img000.png
                - img001.png
            - test/
                - img000.png
                - img001.png

    Args:
        root (string): root directory of the dataset
        transform (callable, optional): a function or transform that takes in a
            PIL image and returns a transformed version
        split (string): split mode ('train' or 'val')
    """

    def __init__(self, root, transform=None, split="train", mode="train", rate = 1.0):
        splitdir = Path(root) / split

        if not splitdir.is_dir():
            raise RuntimeError(f'Invalid directory "{root}"')

        self.samples = [f for f in splitdir.iterdir() if f.is_file()]
        self.samples = self.samples[0:(int)(len(self.samples)*rate + 1)]

        self.transform = transform

        self.mode = mode

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            img: `PIL.Image.Image` or transformed `PIL.Image.Image`. - to feature
        """
        read_path = self.samples[index]
        features = feat2feat(read_path)
        p2_feature = features["p2"]
        p3_feature = features["p3"]
        p4_feature = features["p4"]
        p5_feature = features["p5"]
        # print(">>>> p2_feature: ", p2_feature.size())
        # add for img shape bigger
        
        p2_feature = torch.squeeze(p2_feature, 0)
        if self.mode == "train":
            if p2_feature.shape[1] < 128:
                rand_x_start = 0
                rand_x_end = p2_feature.shape[1]
                print("error in feature:ori_p2_feature:{}".format(p2_feature.shape))
            else:
                rand_x_start = (int)(random.random()*(p2_feature.shape[1] - 128))
                rand_x_end = rand_x_start + 128
            #
            if p2_feature.shape[2] < 128:
                rand_y_start = 0
                rand_y_end = p2_feature.shape[2]
                print("error in feature:ori_p2_feature:{}".format(p2_feature.shape))
            else:
                rand_y_start = (int)(random.random()*(p2_feature.shape[2] - 128))
                rand_y_end = rand_y_start + 128
            #
            p2_feature_new = torch.zeros([p2_feature.shape[0], 128, 128])
            p2_feature_new[:, 0:(rand_x_end - rand_x_start), 0:(rand_y_end - rand_y_start)] = p2_feature[:, rand_x_start:rand_x_end, rand_y_start:rand_y_end]
            p2_feature = p2_feature_new
            # p3_feature = torch.squeeze(p3_feature, 0)
            # p4_feature = torch.squeeze(p4_feature, 0)
            # p5_feature = torch.squeeze(p5_feature, 0)
            return p2_feature,str(read_path)#,p3_feature,p4_feature,p5_feature,str(read_path)
        elif self.mode == "test":
            #test
            p3_feature = torch.squeeze(p3_feature, 0)
            p4_feature = torch.squeeze(p4_feature, 0)
            p5_feature = torch.squeeze(p5_feature, 0)
            return p2_feature,p3_feature,p4_feature,p5_feature,str(read_path)
        
        elif self.mode == "test_multi":
            p3_feature = torch.squeeze(p3_feature, 0)
            p4_feature = torch.squeeze(p4_feature, 0)
            p5_feature = torch.squeeze(p5_feature, 0)
            return p2_feature,p3_feature,p4_feature,p5_feature,str(read_path)


        # # 按通道拼
        # # add for split merge
        # p2_feature, mode = _joint_split_features(p2_feature, out_channels = 3)
        # p2_feature = torch.from_numpy(p2_feature)
        # # add by ywz
        # p2_feature = torch.squeeze(p2_feature, 0)
        # return p2_feature


    def __len__(self):
        return len(self.samples)
