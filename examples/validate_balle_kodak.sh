#!/usr/bin/env bash

#python3 examples/validation.py -m bmshj2018-hyperprior -d /home/jjp/Dataset/kodak128/ -q 1 --lambda 0.00045 --batch-size 6 -lr 1e-4 --save --cuda --exp CLIC_balle_En_0.00045 --checkpoint /home/jjp/CompressAI/pretrained_model/bmshj2018-hyperprior/bmshj2018-hyperprior-1-7eb97409.pth.tar --en /home/jjp/CompressAI/experiments/exp_balle_En_0.00045/checkpoints/En_net_checkpoint_best_loss.pth.tar ||
#wait
python3 examples/validation.py -m bmshj2018-hyperprior -d /home/jjp/Dataset/kodak128/ -q 2 --lambda 0.0009 --batch-size 6 -lr 1e-4 --save --cuda --exp kodak_balle_En_0.0009 --checkpoint /home/jjp/CompressAI/pretrained_model/bmshj2018-hyperprior/bmshj2018-hyperprior-2-93677231.pth.tar --en /home/jjp/CompressAI/experiments/exp_balle_En_0.0009/checkpoints/En_net_checkpoint_best_loss.pth.tar ||
#wait
python3 examples/validation.py -m bmshj2018-hyperprior -d /home/jjp/Dataset/kodak128/ -q 3 --lambda 0.0016 --batch-size 6 -lr 1e-4 --save --cuda --exp kodak_balle_En_0.0016 --checkpoint /home/jjp/CompressAI/pretrained_model/bmshj2018-hyperprior/bmshj2018-hyperprior-3-6d87be32.pth.tar --en /home/jjp/CompressAI/experiments/exp_balle_En_0.0016/checkpoints/En_net_checkpoint_best_loss.pth.tar ||
#wait
python3 examples/validation.py -m bmshj2018-hyperprior -d /home/jjp/Dataset/kodak128/ -q 4 --lambda 0.0033 --batch-size 6 -lr 1e-4 --save --cuda --exp kodak_balle_En_0.0033 --checkpoint /home/jjp/CompressAI/pretrained_model/bmshj2018-hyperprior/bmshj2018-hyperprior-4-de1b779c.pth.tar --en /home/jjp/CompressAI/experiments/exp_balle_En_0.0033/checkpoints/En_net_checkpoint_best_loss.pth.tar ||
#wait
python3 examples/validation.py -m bmshj2018-hyperprior -d /home/jjp/Dataset/kodak128/ -q 5 --lambda 0.0063 --batch-size 6 -lr 1e-4 --save --cuda --exp kodak_balle_En_0.0063 --checkpoint /home/jjp/CompressAI/pretrained_model/bmshj2018-hyperprior/bmshj2018-hyperprior-5-f8b614e1.pth.tar --en /home/jjp/CompressAI/experiments/exp_balle_En_0.0063/checkpoints/En_net_checkpoint_best_loss.pth.tar ||
#wait
python3 examples/validation.py -m bmshj2018-hyperprior -d /home/jjp/Dataset/kodak128/ -q 6 --lambda 0.012 --batch-size 6 -lr 1e-4 --save --cuda --exp kodak_balle_En_0.012 --checkpoint /home/jjp/CompressAI/pretrained_model/bmshj2018-hyperprior/bmshj2018-hyperprior-6-1ab9c41e.pth.tar --en /home/jjp/CompressAI/experiments/exp_balle_En_0.012/checkpoints/En_net_checkpoint_best_loss.pth.tar ||
